package com.bcs.codgen.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import org.apache.commons.lang.StringUtils;

import com.bcs.codgen.model.ForeignKeyModel;
import com.bcs.codgen.model.JdbcConfig;
import com.bcs.codgen.model.PrimaryKeyModel;
import com.bcs.codgen.service.ColumnHandler;
import com.bcs.codgen.service.DbProvider;
import com.bcs.codgen.util.JdbcUtil;

/**
 * 针对Oracle的数据库信息提供者
 * @author 黄天政
 *
 */
public class OracleProvider extends DbProvider {
	private static final long serialVersionUID = -4293483654633811103L;

	public OracleProvider(Connection conn) {
		super(conn);
	}

	public OracleProvider(JdbcConfig jdbcConfig) {
		super(jdbcConfig);
	}

	/**
	 * 统一设置大写格式的表名模式
	 */
	@Override
	public void setTableNamePatterns(String tableNamePatterns) {
		if(StringUtils.isNotBlank(tableNamePatterns)){
			tableNamePatterns = tableNamePatterns.toUpperCase();
		}
		super.setTableNamePatterns(tableNamePatterns);
	}

	@Override
	protected Map<String, String> doGetColumnComments(String tableName) {
		Map<String, String> colComment = new LinkedHashMap<String, String>();
		String columnName = null, comment = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select * from USER_COL_COMMENTS where TABLE_NAME='"+tableName.toUpperCase()+"'";
		try{
			stmt = getConn().createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				columnName = rs.getString("COLUMN_NAME").toLowerCase();
				comment = StringUtils.trim(rs.getString("COMMENTS"));
				colComment.put(columnName, comment);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			JdbcUtil.safelyClose(rs, stmt);
		}
		return colComment;
	}

	@Override
	protected Map<String, String> doGetTableComments() {
		Map<String, String> tableComments = new LinkedHashMap<String, String>();
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "select * from USER_TAB_COMMENTS";
		String tnps = getTableNamePatterns();
		if(StringUtils.isNotBlank(tnps)){
			List<String> conditions = new ArrayList<>();
			for (String tnp : tnps.split(",")) {
				conditions.add("TABLE_NAME LIKE '%"+tnp+"%'");
			}
			sql += " WHERE " + StringUtils.join(conditions, " OR ");
		}
		try{
			stmt = getConn().createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				tableComments.put(rs.getString("TABLE_NAME").toLowerCase(), rs.getString("COMMENTS")) ;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			JdbcUtil.safelyClose(rs, stmt);
		}
		return tableComments;
	}

	/**
	 * 构建查询指定表中一条记录的SQL语句（不同数据库方言语法可能不同）
	 *
	 * @param tableName 表名
	 * @return 可以执行查询返回一条记录的sql语句
	 */
	@Override
	protected String buildSql4OneRecord(String tableName) {
		return "select * from " + tableName + " where rownum = 1";
	}

	/* (non-Javadoc)
	 * @see com.bcs.codgen.service.DbProvider#getPrimaryKeys(java.lang.String)
	 */
	@Override
	protected List<PrimaryKeyModel> getPrimaryKeys(String tableName) {
		//oracle在此方法调用需要大写表名
		tableName = StringUtils.upperCase(tableName);
		return super.getPrimaryKeys(tableName);
	}

	/* (non-Javadoc)
	 * @see com.bcs.codgen.service.DbProvider#getImportedKeys(java.lang.String)
	 */
	@Override
	protected List<ForeignKeyModel> getImportedKeys(String tableName) {
		//oracle在此方法调用需要大写表名
		tableName = StringUtils.upperCase(tableName);
		return super.getImportedKeys(tableName);
	}

	/* (non-Javadoc)
	 * @see com.bcs.codgen.service.DbProvider#getExportedKeys(java.lang.String)
	 */
	@Override
	protected List<ForeignKeyModel> getExportedKeys(String tableName) {
		//oracle在此方法调用需要大写表名
		tableName = StringUtils.upperCase(tableName);
		return super.getExportedKeys(tableName);
	}


}
